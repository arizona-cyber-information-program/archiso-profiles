First: Name scheme
Second: Description

# Name Scheme

##### Base profiles
Desktop enviornments alone are the base for each other the others.  
Example:  
* kde
* cinnamon
* gnome  

##### New profiles
The base profile should always preceed the description.  
Further descriptions should also be separated with an underscore *not* a space.  
Example:  
* cinnamon\_hardened
* kde\_forensic

# Description
These are profiles used to build arch ISO files.  
This is a fork of archuseriso, thus uses their instructions as a base. Thus this repository is also a fork of archuseriso.  
The tool is useful in that the scripts provide more functionality for the USBs rather than the normal tool "archiso".  
Feel free to use these profiles for either archiso or archuseriso. Currently, the use case is better seen with archuseriso, though.  

